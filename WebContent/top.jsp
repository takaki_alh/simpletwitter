<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="//code.jquery.com/jquery-3.2.1.min.js"></script>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>簡易Twitter</title>
</head>
<body>
    <div class="main-contents">
        <div class="header">
            <c:if test="${empty loginUser }">
                <a href="login">ログイン</a>
                <a href="signup">登録する</a>
            </c:if>
            <c:if test="${not empty loginUser }">
                <a href="./">ホーム</a>
                <a href="setting">設定</a>
                <a href="logout">ログアウト</a>
            </c:if>
        </div>

        <!-- つぶやきの絞り込み -->
        <div class="message-serch">
            <form action="./" method="get">
                日付 <span><input name="start" type="date" value="${start}"></span> ～ <span><input name="end" type="date" value="${end}"></span>
                <span><input type="submit" value="絞込"></span>
            </form>
        </div>

        <!-- ログインしているユーザーの情報 -->
        <c:if test="${not empty loginUser }">
            <div class="prifile">
                <div class="name">
                    <h2>
                        <c:out value="${loginUser.name }" />
                    </h2>
                </div>
                <div class="account">
                    @<c:out value="${loginUser.account }" />
                    <script>
                            console.log(${loginUser.account});
                    </script>
                </div>
                <div class="description">
                    <c:out value="${loginUser.description }" />
                </div>
            </div>
        </c:if>

        <!-- メッセージのつぶやきフォーム -->
        <c:if test="${not empty errorMessages }">
            <div class="errorMessages">
                <ul>

                    <c:forEach items="${errorMessages }" var="errorMessage">
                        <li><c:out value="${errorMessage }" />

                    </c:forEach>
                </ul>
            </div>
            <c:remove var="errorMessages" scope="session" />
        </c:if>

        <div class="form-area">
            <c:if test="${isShowMessageForm }">
                <form action="message" method="post">
                    いま、どうしてる？<br />
                    <textarea name="text" cols="100" rows="5" class="tweet-box"></textarea>
                    <br /> <input id="tweet-button" type="submit" value="つぶやく">(140字まで)
                </form>
            </c:if>
        </div>

        <!-- メッセージの表示 -->
        <div class="messages">
            <c:forEach items="${messages}" var="message">
                <div class="message">
                    <div class="account-name">
                        <span class="account"><a href="./?user_id=<c:out value="${message.userId}" />"> <c:out value="${message.account }" /></a></span>
                        <span class="name"><c:out value="${message.name}" /></span>
                    </div>
                    <div class="text">
                        <pre><c:out value="${message.text}" /></pre>
                    </div>
                    <div class="date">
                        <fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" />
                    </div>
                    <!-- 投稿したメッセージがユーザーと一致した場合「編集」・「削除」ボタンを表示する -->
                    <c:if test="${message.userId == loginUser.id}">
                        <div class="edit-delete">
                            <div class="edit" style="display: inline-block;">
                                <form action="edit" method="get">
                                    <input name="message_id" value="${message.id}" id="message_id" type="hidden">
                                    <input style="display: inline;" type="submit" value="編集">
                                </form>
                            </div>
                            <div class="delete" style="display: inline-block;">
                                <form action="deleteMessage" method="post">
                                    <input name="message_id" value="${message.id}" id="message_id" type="hidden">
                                    <input id="delete-button" style="display: inline;" type="submit" value="削除">
                                </form>
                            </div>
                        </div>
                    </c:if>
                </div>
                <!-- コメントフォーム -->
                <c:if test="${not empty loginUser }">
                    <div class="comment-form">
                        <form action="comment" method="post">
                            返信<br />
                            <input name="message_id" value="${message.id }" id="message_id" type="hidden">
                            <input name="id" value="${user.id}" id="id" type="hidden">
                            <textarea name="text" cols="100" rows="5" class="comment-box"></textarea>
                            <br /> <input type="submit" value="返信">(140字まで)
                        </form>
                    </div>
                </c:if>
                <!-- コメント（返信）表示 -->
                <c:forEach items="${comments}" var="comment">
                    <c:if test="${message.id == comment.messageId }">
                        <div class="comment" style="border: 1px red solid">
                            <div class="account-name">
                                <span class="account"><c:out value="${comment.account}" /></span>
                                <span class="name"><c:out value="${comment.name}" /></span>
                            </div>
                            <div class="text">
                                <pre><c:out value="${comment.text}" /></pre>
                            </div>
                            <div class="date">
                                <fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" />
                            </div>
                        </div>
                    </c:if>
                </c:forEach>
            </c:forEach>
        </div>


        <div class="copyright">Copyright(c)Takaki Makoto</div>
    </div>
    <script src="./js/top.js"></script>
</body>
</html>