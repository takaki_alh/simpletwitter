<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<script src="//code.jquery.com/jquery-3.2.1.min.js"></script>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>つぶやき編集画面</title>
</head>
<body>
    <div class="main-contents">
        <!-- ログインしているユーザーの情報 -->
        <c:if test="${not empty loginUser }">
            <div class="prifile">
                <div class="name">
                    <h2>
                        <c:out value="${loginUser.name }" />
                    </h2>
                </div>
                <div class="account">
                    @<c:out value="${loginUser.account }" />
                </div>
                <div class="description">
                    <c:out value="${loginUser.description }" />
                </div>
            </div>
        </c:if>

        <c:if test="${not empty errorMessages }">
            <div class="errorMessages">
                <ul>
                    <c:forEach items="${errorMessages }" var="errorMessage">
                        <li><c:out value="${errorMessage }" />
                    </c:forEach>
                </ul>
            </div>
            <c:remove var="errorMessages" scope="session" />
        </c:if>

        <div class="form-area">
            <form action="edit" method="post">
                <br />
                <input name="id" value="${message.id }" id="message_id" type="hidden" />
                <label for="message">つぶやき</label>
                <br />
                <textarea name="text" cols="100" rows="5" class="tweet-box"><c:out value="${message.text}" /></textarea>
                <br />
                <input type="submit" value="更新">(140文字まで)
            </form>
            <p style="margin-top: 10px">
                <a href="./">戻る</a>
            </p>
        </div>


        <div class="copyright">Copyright(c)Takaki Makoto</div>
    </div>
</body>
</html>