-- MySQL dump 10.13  Distrib 5.6.11, for Win64 (x86_64)
--
-- Host: localhost    Database: simple_twitter
-- ------------------------------------------------------
-- Server version	5.6.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (24,'返信１',40,37,'2021-05-26 02:51:11','2021-05-26 02:51:11'),(25,'返信２',40,37,'2021-05-26 02:51:14','2021-05-26 02:51:14'),(26,'返信',40,37,'2021-05-26 02:57:24','2021-05-26 02:57:24'),(27,'返信\r\n改行',40,37,'2021-05-26 02:57:38','2021-05-26 02:57:38'),(28,'hensin',41,38,'2021-05-26 05:46:45','2021-05-26 05:46:45'),(29,'hensin2',41,38,'2021-05-26 05:46:49','2021-05-26 05:46:49'),(30,'返信',41,38,'2021-05-26 05:47:07','2021-05-26 05:47:07'),(31,';;',40,41,'2021-05-26 06:05:34','2021-05-26 06:05:34'),(32,'kai\r\ngyou',40,41,'2021-05-26 06:05:55','2021-05-26 06:05:55'),(33,'kk',40,43,'2021-05-26 07:38:49','2021-05-26 07:38:49'),(34,'kai\r\ngyou',40,43,'2021-05-26 07:38:58','2021-05-26 07:38:58'),(35,'hensin',40,47,'2021-05-27 04:04:29','2021-05-27 04:04:29'),(36,'kai\r\ngyou',40,47,'2021-05-27 04:04:52','2021-05-27 04:04:52');
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `text` text NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` VALUES (38,41,'つぶやき２更新　\r\n改行','2021-04-29 15:00:00','2021-05-26 03:00:18'),(40,41,'ｋｋll\r\nkaigyou','2021-05-26 05:47:14','2021-05-26 06:07:59'),(41,42,'aa','2021-06-18 02:44:48','2021-06-18 02:44:48'),(42,49,'test','2021-06-21 01:29:38','2021-06-21 01:29:38');
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(20) NOT NULL,
  `name` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `description` text,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `account` (`account`)
) ENGINE=InnoDB AUTO_INCREMENT=117 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'account1','名前1','test@test','password',NULL,'2021-06-21 04:26:55','2021-06-21 04:26:55'),(2,'account2','名前2','test2@test','password2',NULL,'2021-06-21 04:26:55','2021-06-21 04:26:55'),(3,'account3','名前3','test3@test','password3',NULL,'2021-06-21 04:26:55','2021-06-21 04:26:55'),(93,'account10','名前10','test10@test','password10',NULL,'2021-06-21 04:26:55','2021-06-21 04:26:55'),(94,'account20','名前20','test20@test','password20',NULL,'2021-06-21 04:26:55','2021-06-21 04:26:55'),(95,'account30','名前30','test30@test','password30',NULL,'2021-06-21 04:26:55','2021-06-21 04:26:55'),(116,'NewAccount','新規ユーザー','new-user@test','password',NULL,'2021-06-21 06:22:03','2021-06-21 06:22:03');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-07-30 14:06:09
