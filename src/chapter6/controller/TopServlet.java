package chapter6.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

//import chapter6.beans.Comment;
import chapter6.beans.User;
import chapter6.beans.UserComment;
import chapter6.beans.UserMessage;
import chapter6.service.CommentService;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		boolean isShowMessageForm = false;
		User user = (User) request.getSession().getAttribute("loginUser");
		if (user != null) {
			isShowMessageForm = true;
		}

		String userId = request.getParameter("user_id"); // ユーザーリンク：該当のユーザーのつぶやきを取得するためのIDを取得
		String start = request.getParameter("start"); // 絞込：開始日
		String end = request.getParameter("end"); // 絞込：終了日

		List<UserMessage> messages = new MessageService().select(userId, start, end);

		List<UserComment> comments = new CommentService().select();

		// 絞込：開始日が入力されていた場合は、値を返す
		if (!StringUtils.isBlank(start)) {
			request.setAttribute("start", start);
		}

		if (!StringUtils.isBlank(end)) {
			request.setAttribute("end", end);
		}

		request.setAttribute("messages", messages);
		request.setAttribute("isShowMessageForm", isShowMessageForm);
		request.setAttribute("comments", comments);
		request.getRequestDispatcher("/top.jsp").forward(request, response);

	}
}