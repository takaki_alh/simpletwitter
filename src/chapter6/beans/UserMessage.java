package chapter6.beans;

import java.io.Serializable;
import java.util.Date;

public class UserMessage implements Serializable {

	private int id;
	private String account;
	private String name;
	private int userId;
	private String text;
	private Date createdDate;

	// idのセッター
	public void setId(int id) {
		this.id = id;
	}

	// idのゲッター
	public int getId() {
		return id;
	}

	// accountのセッター
	public void setAccount(String account) {
		this.account = account;
	}

	// accountのゲッター
	public String getAccount() {
		return account;
	}

	// nameのセッター
	public void setName(String name) {
		this.name = name;
	}

	// nameのゲッター
	public String getName() {
		return name;
	}

	// userIdのセッター
	public void setUserId(int userId) {
		this.userId = userId;
	}

	// userIdのゲッター
	public int getUserId() {
		return userId;
	}

	// textのセッター
	public void setText(String text) {
		this.text = text;
	}

	// textのゲッター
	public String getText() {
		return text;
	}

	// createdDateのセッター
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	// createdDateのゲッター
	public Date getCreatedDate() {
		return createdDate;
	}

}
