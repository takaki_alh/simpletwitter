package chapter6.beans;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {

	private int id;
	private String account;
	private String name;
	private String email;
	private String password;
	private String description;
	private Date createdDate;
	private Date updatedDate;

	// ユーザーIDのセッター
	public void setId(int id) {
		this.id = id;
	}

	// ユーザーIDのゲッター
	public int getId() {
		return id;
	}

	// ユーザーアカウントセッター
	public void setAccount(String account) {
		this.account = account;
	}

	// ユーザーアカウントのゲッター
	public String getAccount() {
		return account;
	}

	// ユーザーネームのセッター
	public void setName(String name) {
		this.name = name;
	}

	// ユーザーネームのゲッター
	public String getName() {
		return name;
	}

	// emailのセッター
	public void setEmail(String email) {
		this.email = email;
	}

	// emailのゲッター
	public String getEmail() {
		return email;
	}

	// passwordのセッター
	public void setPassword(String password) {
		this.password = password;
	}

	// passwordのゲッター
	public String getPassword() {
		return password;
	}

	// descriptionのセッター
	public void setDescription(String description) {
		this.description = description;
	}

	// descriptionのゲッター
	public String getDescription() {
		return description;
	}

	// createdDateのセッター
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	//createdDateのゲッター
	public Date getCreatedDate() {
		return createdDate;
	}

	// updatedDateのセッター
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	// updatedDateのゲッター
	public Date getUpdatedDate() {
		return updatedDate;
	}

}
