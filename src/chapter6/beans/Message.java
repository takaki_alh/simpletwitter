package chapter6.beans;

import java.io.Serializable;
import java.util.Date;

public class Message implements Serializable {

	private int id;
	private int userId;
	private String text;
	private Date createdDate;
	private Date updatedDate;

	// idのセッター
	public void setId(int id) {
		this.id = id;
	}

	// idのゲッター
	public int getId() {
		return id;
	}

	// userIdのセッター
	public void setUserId(int userId) {
		this.userId = userId;
	}

	// userIdのゲッター
	public int getUserId() {
		return userId;
	}

	// textのセッター
	public void setText(String text) {
		this.text = text;
	}

	// textのゲッター
	public String getText() {
		return text;
	}

	// createdDateのセッター
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	// createdDateのゲッター
	public Date getCreatedDate() {
		return createdDate;
	}

	// updatedDateのセッター
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	// updatedDateのゲッター
	public Date getUpdatedDate() {
		return updatedDate;
	}

}
